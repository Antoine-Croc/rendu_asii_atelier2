import { combineReducers } from 'redux';
import userReducer from './userReducer';
import cardReducer from './cardReducer';
import duelReducer from './duelReducer';

const globalReducer = combineReducers({
    userReducer: userReducer,
    cardReducer: cardReducer,
    duelReducer:duelReducer,
});

export default globalReducer;
