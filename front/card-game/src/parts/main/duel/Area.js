import CarteShort from "./CarteShort";

function Area(props) {
    let cardsOnBattleField = props.cartesOnBattlefield;
    let displayCardsAdv = <div className="col-sm-3 arenaFiel"></div>;
    
    let displayCardsUser;
    console.log(cardsOnBattleField)
    if(cardsOnBattleField.othercards != undefined || cardsOnBattleField.owncards != undefined) 
    {
        displayCards(cardsOnBattleField);
    }
    
    function displayCards(cardsOnBattleField) 
    {
        displayCardsAdv = cardsOnBattleField.othercards.map(
            (carte) => <div className="col-sm-3 arenaFiel" onClick={() => {props.onSelectCarteOnBattlefield(carte.id)}}><CarteShort carte={carte}></CarteShort></div>)
        
        displayCardsUser = cardsOnBattleField.owncards.map(
            (carte) => <div className="col-sm-3 arenaFiel"><CarteShort carte={carte}></CarteShort></div>)
    }
    console.log(displayCardsAdv)
    return(
        <div  id="theField">
            
        <div className="row adversField">
            {displayCardsAdv}   
        </div>
        <div className="row yourField">   
            {displayCardsUser}
        </div>
        </div>
        
    )
}
export default Area;