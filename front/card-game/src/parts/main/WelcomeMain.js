import {Grid,Icon,Image} from 'semantic-ui-react'
import HeaderPage  from'../header/HeaderPage';
import {useSelector} from 'react-redux';
import { NavLink } from "react-router-dom";

function Welcome(props){
    let user = useSelector(state => state.userReducer.current_user)
    let title = props.title
    return(
    <div className="container">
        <div className="row">
            <HeaderPage user={user} title={title}></HeaderPage>
        </div>
        <Grid textAlign = 'center' centered padded='vertically'>
            <Grid.Row centered columns={2}>
                <Grid.Column textAlign = 'center'>
                    <Icon name='eur' size = 'big'/>
                    <NavLink to="/market" ><h1>Acheter</h1></NavLink>
                </Grid.Column>
                <Grid.Column textAlign = 'center'>
                    <Icon name='folder' size = 'big'/> 
                    <NavLink to="/collection" ><h1>Collection/Vendre</h1></NavLink>        
                </Grid.Column>
            </Grid.Row>
            <Grid.Row centered>
                    <Icon name='gamepad' size = 'big'/>
                    <NavLink to="/duel"><h1>Jouer</h1></NavLink>  
            </Grid.Row>
        </Grid>
    </div>
    )
}

export default Welcome;