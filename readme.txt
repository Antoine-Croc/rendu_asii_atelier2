Atelier II du projet d'ASI2 réalisé par Clément Cornu, Philippe Charrat et Antoine Crocquevieille.

Activités:
-Séance 1:
	Philippe : Fin de mise en place du backend atelier 1
	Antoine & Clément: Prise en main sujet atelier 2 + début phase A

-Séance 2:
	Philippe : Programmation duel entre 2 joueurs (front react et back node)
	Antoine : Fetch user from backend
	Clément : Fin programmation chat node + front html 

- Séance 3:
	Philippe : Programmation duel entre 2 joueurs (front react et back node) (suite)
	Antoine : Sauvegarde de l'historique des conversations du chat
	Clément : Back Spring log messages du bus de communication

- Séance 4:
	Philippe : Programmation duel entre 2 joueurs (front react et back node) (fin)
	Antoine : Sauvegarde de l'historique des conversations du chat (suite)
	Clément : intégration chat dans react


https://gitlab.com/Antoine-Croc/rendu_asii_atelier2

Eléments réalisés:
Phases A à D


Eléments non-réalisés:
Phases E et F

Remarque : 
  - Création d'utilisateur uniquement via Postman
  - Création de nouvelle carte uniquement via Postman
  - Rafraichissement d'une page entraine la déconnexion
  - Part Market/Collection : 
	* Erreur d'affichage : cartes non supprimé de la view après achat (manque un refresh)
  - Part Game :
	* Le créateur de la room impose son bet, le second doit miser la même. Manque d'un test de validation de cagnotte utilisateur et d'une liste de salle "vide" avec leur pari associé. 
	* Sélection d'une carte via Checkbox fonctionne mais est invisible. L'élement reste vide, il faut utiliser la console pour vérifier que la carte a bien été prise.
	* Pas d'aléatoire sur le premier jouer, le créateur commence.
	* Jeu du type Magic/Yugioh : le but est de faire tomber le point de vie adverse à zéro.
	* Node : Pas une bonne gestion des sockets, simple broadcast avec filtrage par idRoom
	* Utilisation de topics et non de queue.
  - Chat : La partie d'enregistrement des messages et récupération était réalisé en ajax, suite au changement en node(demandé dans l'énoncé), l'ajax a du être retiré car il créait des prolèmes vis a vis des librairies nodes. (voir screens dans le git)
 
Possibilité d'erreur après regroupement des gits. Les gits originaux sont : 
  - Node (Game Manager) : https://gitlab.com/charrat.p/node_game_manager
  - Node (tchat) : https://gitlab.com/clementcornu/nodejs_asi_2.git/
  - Front : https://gitlab.com/charrat.p/projet_asiii (branch : advanced_part_philippe)
  - Back : https://gitlab.com/charrat.p/back_card_game
  - 
