import React,{ useState } from 'react';
import { useEffect } from 'react'
import axios from 'axios'
import Carte from './Carte';
import ListeCarte from './ListeCarte';
import  {useSelector} from 'react-redux';
import CarteAndForm from './CarteAndForm';

function BodyCard(props) {

  let user = useSelector(state => state.userReducer.current_user)
  let url;
  const [cartes,setCartes] = useState([])
  let type = props.title;
  const [id_carte, changeId] = useState(0);
 
  const onSelectCarte = (id) => {
    changeId(id)
  }

  function isReadyToDisplay(){
    let size = Object.keys(user.cardList).length;
    if (size== cartes.length){
      setCartes([...cartes]);
    }
  }

  useEffect (() => {
    if(type == "MARKET")
    {
      url = 'http://127.0.0.1:8082/cards_to_sell';
      console.log("Récupération Market")
      axios.get(url).then(res => {
        setCartes(res.data);
      });
    }
    else
    {
      url = 'http://127.0.0.1:8082/card/';
      for(var id of user.cardList)
      {
        axios.get(url+id).then(res => {
          cartes.push(res.data);
          isReadyToDisplay();
        });
      }
    }
  },[type]);

  return (
    <div className="row">
      <div className="col-sm-8">
        <ListeCarte cartes={cartes} onSelectCarte={onSelectCarte}></ListeCarte>
      </div>
      <div className="col-sm-4">
        <CarteAndForm id={id_carte} type={type} cartes={cartes}></CarteAndForm>
      </div>
    </div>
  );
}
  
export default BodyCard;