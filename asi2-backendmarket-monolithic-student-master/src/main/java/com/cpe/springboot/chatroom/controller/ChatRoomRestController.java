package com.cpe.springboot.chatroom.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.chatroom.model.ChatRoomModel;
import com.cpe.springboot.user.controller.UserService;

//Décommenter si changement de méthode pour getRoomUsersIds
//import com.cpe.springboot.user.model.UserModel;

@RestController
public class ChatRoomRestController {

	private final ChatRoomService crService;
	
	//Décommenter si changement de méthode pour getRoomUsersIds
	//private final UserService uService;
	
	public ChatRoomRestController(ChatRoomService chatroomService, UserService userService) {
		this.crService = chatroomService;
		//Décommenter si changement de méthode pour getRoomUsersIds
		//this.uService = userService;		
	}
	
	//TODO ADD TO BUS QUEUE
	@RequestMapping(method = RequestMethod.POST, value = "/chat")
	public void createRoom(@RequestBody Integer Id1, @RequestBody Integer Id2) {
		crService.addRoom(Id1, Id2);
	}
	//TODO ADD TO BUS QUEUE
	@RequestMapping(method = RequestMethod.GET, value = "/chat")
	public ArrayList<ChatRoomModel> getAllRooms(){
		return crService.getAllRooms();
	}
	
	//TODO ADD TO BUS QUEUE
	@RequestMapping(method = RequestMethod.GET, value ="/chat/{roomId}")
	public List<Integer> getRoomUsersIds(@PathVariable String roomId) {
		ChatRoomModel cr = crService.getRoom(roomId);
		List<Integer> uL = cr.getRoomUsers();
		/* Pour appeler la liste des users au lieu des IDs on utilisera la méthode ci-dessous
		 * 
		 * ArrayList<UserModel> umL = new ArrayList<>(); for (int i=0; i < uL.size();
		 * i++) { umL.add(uService.getUser(uL[i])); }
		 */
		return uL;
	}
	
	
}
