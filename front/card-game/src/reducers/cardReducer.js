const cardReducer= (state={card:{}, cardlist:{}},action) => {

    switch (action.type) {
        case 'SELECTED_CARD_ACTION':
            return {card:action.card};
        case 'UPDATE_CARDS_ACTION':
            return {cardlist:action.cardlist}
    default:
      return state;
    }
}

export default cardReducer;