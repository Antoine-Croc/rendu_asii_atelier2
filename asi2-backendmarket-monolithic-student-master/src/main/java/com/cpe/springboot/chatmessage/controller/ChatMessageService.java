package com.cpe.springboot.chatmessage.controller;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cpe.springboot.chatmessage.model.ChatMessageModel;

@Service
public class ChatMessageService {
	
	private final ChatMessageRepository cmRepository;
	
	public ChatMessageService(ChatMessageRepository chatmessageRepository) {
		this.cmRepository = chatmessageRepository;
	}
	
	public void addMessage(String msg, Integer roomId, Integer userId, String TimeStamp) {
		ChatMessageModel cm = new ChatMessageModel(msg,TimeStamp, roomId, userId);
		cmRepository.save(cm);
	}
	
	public List<ChatMessageModel> getAllUserMsg(Integer userId) {
		return cmRepository.findByUserId(userId);
	}
	
	/*
	 * public List<ChatMessageModel> getAllRoomMsg(Integer roomId) { return
	 * cmRepository.findByRoomId(roomId); }
	 */
		
	public void deleteMessage(Integer msgId) {
		cmRepository.deleteById(msgId);
	}
	
	
}
