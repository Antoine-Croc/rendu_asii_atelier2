import axios from "axios";

function AdversInfo(props) {
    
    function hitAplayerButton(event)
    {
        event.preventDefault();
        const order = {idPlayer: props.idUser,idRoom:props.idRoom };  
        console.log(order)  
        axios.post('http://127.0.0.1:4000/hitaPlayer', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})  
        .then(res => {console.log(res.data);  })
    }
    let heart;  
    if(!props.hitheart)
    {
        heart = <i className="fa fa-heart"></i>
    }
    else 
    {
        heart = <i className="fa fa-heart hit"></i>    
    }
    console.log(heart)

    return(
        <div className="row adversInfo">
            <div className="col-sm-3 arenaFiel"><img id="pdprofilDuel" src="https://sbcf.fr/wp-content/uploads/2018/03/sbcf-default-avatar.png"></img></div>
            <div className="col-sm-3 arenaFiel"><center><form onSubmit={hitAplayerButton}><input type="submit" value="Attaquer l'adversaire"></input></form></center></div>
            <div className="col-sm-3 arenaFiel">PDV : {heart}</div>
            <div className="col-sm-3 arenaFiel">The BET </div>   
        </div>
        
    )
}
export default AdversInfo;