function CarteShort(props) {
    let carte = props.carte;
    return(
    <div className="card cardOnField">
                <div className="">
                    <div className="row">
                            <a className="ui red circular label">{carte.attack}</a>
                    </div>
                    <div className="row" >
                            <h5>{carte.name}</h5>
                    </div>
                    <div className="row">
                            <a className="ui yellow circular label">{carte.hp}</a>
                    </div>
        </div>
    </div>
)
}
export default CarteShort;