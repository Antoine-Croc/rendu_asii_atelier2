import axios from 'axios';
import { NavLink } from 'react-router-dom';
import Carte from '../Carte';
import { useDispatch } from 'react-redux';
import {updateDeck} from '../../../actions/index';
import React,{ useState} from 'react';

function DeckBuilder(props) {
    const dispatch=useDispatch();
    let cards = props.cartes;
    let idRoom = props.idRoom;
    let idPlayer = props.idPlayer;
    let decks = [];
    let cardsForm;
    let choose;
    let carteFormSubmit;
    const [result, setResult] = useState();

    if(cards != undefined)
    {
        choose = <center><h2>Choose your deck</h2></center>
        cardsForm = cards.map((carte) => <div className="formDeckBuilding" key={carte.id} ><Carte carte={carte}></Carte><input type="checkbox" value={carte.id} onClick={addCardsOnDeck} ></input></div>);
        carteFormSubmit = <center><input type="submit"></input></center>;
    } 
    else 
    {
        choose = <center><h2>Wait an opposant</h2></center>
    }

    function sendDeck(event){
        event.preventDefault();
        if(decks.length > 4)
        {
            const order = {idRoom:idRoom, idPlayer: idPlayer, deckPlayer:decks};
            console.log(order)
            axios.post('http://127.0.0.1:4000/chooseADeck', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
            .then(res => {
                console.log(res.data);
                if(res.data)
                {
                    setResult(<div><span id="authentificationSucess">Deck Accepted, Go to the room : </span><NavLink to="/match" >here</NavLink></div>)
                }
                else
                {
                    setResult(<div><span id="authentificationFail">Deck not Accepted</span></div>)
                }
            })
        }
        else 
        {
            alert("La taille de votre deck est insuffisante. Minimum 5")
        }
    }

    function addCardsOnDeck(event){
        event.preventDefault();
        console.log(event.target.value)
        let id = event.target.value;

        let addCardOnDeck = cards.filter(card => card.id == id)[0];
        console.log("Add card on the deck"+id)
        console.log(addCardOnDeck)
        console.log(!(decks.includes(addCardOnDeck)))
        if(!(decks.includes(addCardOnDeck)))
        {
            decks.push(addCardOnDeck);
        }
        else if(decks.includes(addCardOnDeck))
        {
            decks.splice(decks.indexOf(addCardOnDeck),1);
        }
        console.log(decks)
    }

    return(
        <div>
            <div className='row'>
                {choose}    
            </div>
            <div className='row'>
                <form id="formDeckBuilder" onSubmit={sendDeck}>
                    {cardsForm}
                    {carteFormSubmit}
                </form>
            </div>
            <center>
                {result}
            </center>
        </div>
    )
}
export default DeckBuilder;