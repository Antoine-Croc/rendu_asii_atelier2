package com.cpe.springboot.chatroom.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.chatroom.model.ChatRoomModel;


public interface ChatRoomRepository extends CrudRepository<ChatRoomModel, Integer>{
	
	public List<ChatRoomModel> findById(int id);
	
}