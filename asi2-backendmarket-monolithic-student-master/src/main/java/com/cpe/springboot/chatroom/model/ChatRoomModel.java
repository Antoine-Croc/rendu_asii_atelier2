package com.cpe.springboot.chatroom.model;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class ChatRoomModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer firstId;
	private Integer secondId;
	public ChatRoomModel(Integer Id1, Integer Id2) {
		this.firstId = Id1;
		this.secondId = Id2;
	}

	public Integer getRoomId() {
		return this.id;
	}
	
	public List<Integer> getRoomUsers(){
		 List<Integer> uL = new ArrayList<>();
		 uL.add(firstId);
		 uL.add(secondId);
		 return uL;
		 
	}
	
	public void setRoomUsers(Integer IdOne, Integer IdTwo) {
		this.firstId = IdOne;
		this.secondId = IdTwo;
	}	
}
