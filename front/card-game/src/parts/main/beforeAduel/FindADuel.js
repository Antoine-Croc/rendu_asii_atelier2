import axios from 'axios';
import {useSelector} from 'react-redux';
import React,{ useState, useEffect } from 'react';
import {io} from 'socket.io-client'
import { 
    useNavigate
 } from "react-router-dom";
import DeckBuilder from './DeckBuilder';
import { useDispatch } from 'react-redux';
import {updateIDRoom} from '../../../actions/index';
 
let socket;

function FindADuel(props) {  
    const dispatch=useDispatch();
    const [idRoom, setARoom] = useState();
    const [idRoomForm, setAFormRoom] = useState();
    const [displayResultWithoutId, setDiplayOut] = useState(0);
    const [displayResultWithId, setDiplayId] = useState(0);
    const [cartes, setCartes] = useState();
    const [bet,setBet]=useState(0);
    let user = useSelector(state => state.userReducer.current_user);

    
    useEffect(() => {
        socket = io("http://localhost:4000",{
            reconnection: true,
            origins: "*",
            transports: ["websocket"]
        }
        );  
    },[]);

    useEffect(() => {
        socket.on("choose your deck for the game", data => {
            if(data.idPlayer == user.id)
            {
                setCartes(data.deck);
            }
        });
      }, []);

    function findARoomWithoutId(event) {
        event.preventDefault();
        if(user.account > bet)
        {
            const order = {idPlayer: user.id,bet:bet};
            console.log(order)
            sendHTTPrequest(order)
        }
        else 
        {
            alert("Crédit insuffisants.")
        }
    }

    function findARoom(event) {
        event.preventDefault();
        const order = {idPlayer: user.id,idRoom:parseInt(idRoomForm,10)};
        sendHTTPrequest(order);
 
    }

    function sendHTTPrequest(order) {
        axios.post('http://127.0.0.1:4000/beginAMatch', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
        .then(res => {
            let idRoomAxios = res.data.id;
            console.log("Votre room est "+idRoomAxios)
            setARoom(idRoomAxios);
            dispatch(updateIDRoom(idRoomAxios));
            setDiplayId(<div><span id="authentificationSucess">Room trouvée : {idRoomAxios}</span></div>)
        })   
    }

    function handleChange(event) {    
        setAFormRoom(event.target.value);
    }

    
  function handleBetChange (event){
    setBet(event.target.value );
  }

    return(
        <div>
        <div className="row">

            <div className="col-sm-6">
                <form onSubmit={findARoom}>
                    <label htmlFor='findARoomId'>What is your room : </label>
                    <input type="number" id="findARoomId" onChange={handleChange} ></input>
                    <input type="submit" value="find My room"></input>
                </form>
                {displayResultWithId}
            </div>
            <div className="col-sm-6">
                <form onSubmit={findARoomWithoutId}><label htmlFor='bet'>What is your bet</label><input onChange={handleBetChange} type="number" id="bet" required></input><input type="submit" value="Find or create a new room"></input></form>
                {displayResultWithoutId}
            </div>
        </div>
        
        <div className="row">
            <div className="col-sm-1"></div>
            <div className="col-sm-10"><DeckBuilder cartes={cartes} idRoom={idRoom} idPlayer={user.id}></DeckBuilder></div>
            </div>
            <div className="col-sm-1"></div>
            
        </div>
    )
}
export default FindADuel;