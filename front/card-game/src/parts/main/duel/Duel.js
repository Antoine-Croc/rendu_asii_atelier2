import Hand from './Hand'
import HeaderPage from '../../header/HeaderPage';
import {useSelector} from 'react-redux';
import Area from './Area'
import React,{ useEffect, useState } from 'react'
import axios from 'axios'
import Carte from '../Carte';
import AdversInfo from './AdversInfo';
import {io} from 'socket.io-client'
import InformationsDuel from './InformationDuel';
import {updateUser} from '../../../actions';
import Chat from './Chat'
import { useDispatch } from 'react-redux';

let socket;

function Duel() {
    const dispatch=useDispatch();  
    let display;
    let idRoom = useSelector(state => state.duelReducer.idRoom);
    let user = useSelector(state => state.userReducer.current_user)
    const [carte, setCarte] = useState();
    const [pointAction, setActions] = useState(0);
    const [heartHit, setHearth] = useState(false);
    const [cartes,setCartes] = useState([]);
    const [cartesOnBattlefield,setCartesOnBattlefield] = useState({});
    const [selectChoose,setSelect]=useState('');
    
    useEffect(() => {
        socket = io("http://localhost:4000",{
            reconnection: true,
            origins: "*",
            transports: ["websocket"]
        }
        );  
    },[]);

    useEffect(() => {
        socket.on("new turn", data => {
          console.log(data);
          if(data.idPlayer == user.id && data.idRoom == idRoom) {
            setActions(data.actionPoints)
          }
          else {
            setActions(0)
          }
        });
        socket.on("your deck for the game", data => {
            if(data.idPlayer == user.id && data.idRoom == idRoom)
            {
                setCartes(data.deck);
            }
          console.log(data);
        });
        socket.on("endGame", data => {
            if(data.idRoom == idRoom){
                axios.get(`http://127.0.0.1:8082/user/`+user.id)
                  .then(res => { dispatch(updateUser(res.data)); })
            }
            console.log(data);
        });
        socket.on("hit a player", data => {
            if(data.idRoom == idRoom){
                console.log(data);
                setHearth(true);
            }
          
        });
        socket.on("update cards", data => {
            if(data.idRoom == idRoom){
                cardsOnBattlefield(data);
                if(data.idPlayer == user.id){
                    setActions(data.actionPoints);
                }
            }
            });
      }, []);

    useEffect(() => {
        let order = {idRoom:idRoom};
        console.log(idRoom)
        axios.post('http://127.0.0.1:4000/stateOfMatch', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
        .then(res => {
            console.log(res.data);
          })  

        order = {idRoom:idRoom,idPlayer:user.id};
        axios.post('http://127.0.0.1:4000/deck', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
        .then(res => {
            if(res.data != undefined)
            {
                setCartes(res.data);
            }
        })  
    },[]);
  

    function cardsOnBattlefield(data) {
        let newBattlefield = {}
        if(user.id == data.cards.idPlayerOne) 
        {
            newBattlefield.owncards = data.cards.playerOne;
            newBattlefield.othercards = data.cards.playerTwo;
        }
        else        
        {
            newBattlefield.owncards = data.cards.playerTwo;
            newBattlefield.othercards = data.cards.playerOne;
        }
        console.log(newBattlefield)
        setCartesOnBattlefield(newBattlefield);
        console.log(cartesOnBattlefield)
    }
    
    function onSelectCarte(id){
        for(var carteTest of cartes)
        {
            
            if(id == carteTest.id)
            {
                setCarte({carteRecup:carteTest,action:"jouer"});
            }
        }
    }
    
    function onSelectCarteOnBattlefield(id){
        console.log("J'ai cliqué sur une carte")
        for(var carteTest of cartesOnBattlefield.othercards)
        {
            
            if(id == carteTest.id)
            {
                setCarte({carteRecup:carteTest,action:"attack"});
            }
        }
    }

    function isReadyToDisplay(){
        let size = Object.keys(user.cardList).length;
        if (size== cartes.length)
        {
            setCartes([...cartes]);
        }
    }
    
    if(carte != undefined) {
        console.log(carte.action== "jouer")
        if(carte.action == "jouer")
        {
            display=(<div><Carte carte={carte.carteRecup}></Carte><form onSubmit={playACard}><input type="submit" value="La jouer"></input></form></div>);
        }
        else
        {
            let items = []; 
            items.push(<option value=''>-----</option>)        
            for(let element of cartesOnBattlefield.owncards)
            {
                items.push(<option key={"select"+element.id} value={element.id}>{element.name}</option>); 
            }
            display=(<div><Carte carte={carte.carteRecup}></Carte><form onSubmit={attackACard}><label htmlFor="cardStricker">Attaquer avec : </label><select onChange={handleChange} id="cardStricker">{items}</select><input type="submit" value="Attaquer"></input></form></div>);
        }
            
    }
    
    function handleChange(event) {    
        setSelect({value: event.target.value});  
    }

    function attackACard(event){
        event.preventDefault();
    
        const order = {idPlayer: user.id,idcardStricker:parseInt(selectChoose.value),idcardDefenser:carte.carteRecup.id,idRoom:idRoom};
        axios.post('http://127.0.0.1:4000/anAttackIsPlay', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
        .then(res => {
            console.log(res.data);
          })
    }
    function playACard(event) {
        event.preventDefault();
        const order = {idPlayer: user.id, idCard: carte.carteRecup.id,idRoom:idRoom };
        console.log(order)
        axios.post('http://127.0.0.1:4000/playACard', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
    
          .then(res => {
            console.log(res.data);
          })
    }
    return(
        <div>
            <div className="row">
                <HeaderPage user={user} title="DUEL"></HeaderPage>
            </div>
            <div className="row">
                <div className="col-sm-2">
                    <Chat></Chat>
                </div>
                <div className="col-sm-8">
                    <p> The Adversaire : </p>
                    <AdversInfo idUser={user.id} hitheart={heartHit}idRoom={idRoom} ></AdversInfo>
                    <p> The BattelField : </p>
                    <Area cartesOnBattlefield={cartesOnBattlefield} onSelectCarteOnBattlefield={onSelectCarteOnBattlefield}></Area>
                    <p> Information : </p>
                    <InformationsDuel idUser={user.id} pointAction={pointAction} idRoom={idRoom}></InformationsDuel>
                    <p> Your Hand : </p>
                    <Hand cartes={cartes} onSelectCarte={onSelectCarte}></Hand>
                </div>
                <div className="col-sm-2">
                    {display}
                </div>
            </div>
        </div>
            
    )
}
export default Duel;