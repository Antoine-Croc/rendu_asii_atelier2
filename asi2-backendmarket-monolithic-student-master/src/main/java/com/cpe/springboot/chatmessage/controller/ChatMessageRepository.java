package com.cpe.springboot.chatmessage.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.chatmessage.model.ChatMessageModel;

public interface ChatMessageRepository extends CrudRepository<ChatMessageModel, Integer>{
	
	public List<ChatMessageModel> findByUserId(Integer userId);
	
	///public List<ChatMessageModel> findByRoomId(Integer roomId);
	
}
