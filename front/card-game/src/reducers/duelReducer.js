const duelReducer= (state={idRoom:0,deck:[]},action) => {
    switch (action.type) {
        case 'UPDATE_DUEL_ACTION':
            return {idRoom:action.idRoom}
        case 'UPDATE_DECK_ACTION':
                return {deck:action.deck}
    default:
      return state;
    }
}

export default duelReducer;