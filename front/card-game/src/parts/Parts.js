import HeaderPage  from'./header/HeaderPage';
import 'semantic-ui-css/semantic.min.css';
import BodyCard from './main/BodyCard';
import  {useSelector} from 'react-redux';

function Parts(props) {
  let title = props.title

    let user = useSelector(state => state.userReducer.current_user)
  return (
    <div className="container">
      <div className="row">
        <HeaderPage user={user} title={title}></HeaderPage>
      </div>
      <div>
        <BodyCard title={title}></BodyCard> 
      </div>
    </div>
  );
}

export default Parts;
