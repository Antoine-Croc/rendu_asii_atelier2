import  {useSelector} from 'react-redux';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import {updateUser} from '../../actions';
import Carte from './Carte';

function CarteAndForm(props) {
  
  const dispatch=useDispatch();
  
  let id_carte = props.id;
  let cartes = props.cartes;
  let carte ={};
  let displayForm;

  let user = useSelector(state => state.userReducer.current_user);

  function sell_a_card(event) {
    event.preventDefault();

    const order = {user_id: user.id, card_id: id_carte };
    axios.post('http://127.0.0.1:8082/sell', JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})

      .then(res => {
        console.log(res.data);
        axios.get(`http://127.0.0.1:8082/user/`+user.id)
          .then(res => { dispatch(updateUser(res.data)); })
      })
 
  }

  function buy_a_card(event) {
    event.preventDefault();

    const order = {user_id: user.id, card_id: id_carte };
    axios.post('http://127.0.0.1:8082/buy', JSON.stringify(order) , { headers: {  "Content-Type": "application/json"  }})

      .then(res => {
        console.log(res.data);
        axios.get(`http://127.0.0.1:8082/user/`+user.id)
          .then(res => { dispatch(updateUser(res.data)); })
      })
 
  }

  for(var carteTest of cartes)
  {
      if(id_carte == carteTest.id || id_carte == 0)
      {
        id_carte = carteTest.id;
        carte = carteTest;
      }
  }

  if(props.type == "MARKET")
  {
    displayForm = <form method="GET" action="" id="formSoldCard" onSubmit={buy_a_card}>  <input type="hidden" id="idcarte" value={carte.id}></input> <input type="submit" id="submitForm" value="Acheter"></input></form>
  }

  if(props.type == "COLLECTION")
  {
    displayForm = <form method="GET" action="" id="formSoldCard" onSubmit={sell_a_card}>  <input type="hidden" id="idcarte" value={carte.id}></input> <input type="submit" id="submitForm" value="Vendre"></input></form>
  }
      
  return (
    <div>
        <Carte carte={carte}></Carte>
        {displayForm}
    </div>
      );
}

export default CarteAndForm;
