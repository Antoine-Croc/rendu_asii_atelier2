import { useDispatch, useSelector } from 'react-redux'

function ListeCarte(props) {
    let cartes = props.cartes
/*
    let display = Object.keys(cartes).map(
      (key)=>
      <tr onClick={() => {props.onSelectCarte(cartes[key].id)}} key={cartes[key].id}>
            <td>{cartes[key].name}</td>
            <td><img className="smallImage" src={cartes[key].smallImgUrl}></img></td>
            <td>{cartes[key].description}</td>
            <td>{cartes[key].family}</td>
            <td>{cartes[key].affinity}</td>
            <td>{cartes[key].price}</td>
          </tr>
    )
*/
    let display = cartes.map(
        (carte) => 
          <tr onClick={() => {props.onSelectCarte(carte.id)}} key={carte.id}>
            <td>{carte.name}</td>
            <td><img className="smallImage" src={carte.smallImgUrl}></img></td>
            <td>{carte.description}</td>
            <td>{carte.family}</td>
            <td>{carte.affinity}</td>
            <td>{carte.price}</td>
          </tr>
    )

    return (
      <table>
          <thead>
              <tr>
                  <th>Titre</th>
                  <th>Image</th>
                  <th>Description</th>
                  <th>Famille</th>
                  <th>Affinity</th>
                  <th>Price</th>
              </tr>
          </thead>
          {display}
      </table>
    );
  }
  
  export default ListeCarte;