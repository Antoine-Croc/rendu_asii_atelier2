import './App.css';
import MainRouter from './parts/main/MainRouter.js';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import globalReducer from './reducers';
import { useEffect } from 'react';
import axios from 'axios';

//create store to exchange data
const store = createStore(globalReducer);
function App() {
  
  useEffect(()=> {
    console.log("define axios")
    axios.defaults.headers.post["Access-Control-Allow-Origin"] = "*";
    axios.defaults.headers.get["Access-Control-Allow-Origin"] = "*";
  },[])

  return (
    <Provider store={store}>
          <MainRouter></MainRouter>
    </Provider>
  );
}

export default App;
