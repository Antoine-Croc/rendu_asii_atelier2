package com.cpe.springboot.chatmessage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.user.controller.BusService;

@CrossOrigin
@RestController
public class ChatMessageRestController {

	
	@Autowired
	BusService busService;
	
	private final ChatMessageService cmService;
	
	public ChatMessageRestController(ChatMessageService chatMessageService) {
		this.cmService = chatMessageService;
	}

	
	//TODO ADD TO BUS QUEUE
	@RequestMapping(method = RequestMethod.POST, value="/chat/{roomId}")
	public void addMessage(@PathVariable Integer roomId, @RequestBody String msg, 
			@RequestBody Integer userId, String timeStamp) {
		cmService.addMessage(msg, roomId, userId, timeStamp);
	}

	/*
	 * @RequestMapping(method = RequestMethod.) public
	 */
	
}
