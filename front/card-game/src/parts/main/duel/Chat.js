import  {useSelector} from 'react-redux';
import React,{ useEffect, useState } from 'react';
import socketClient  from "socket.io-client"
let socket;

function Chat(props) {
    let user = useSelector(state => state.userReducer.current_user)
    useEffect(() => {
     socket = socketClient("http://127.0.0.1:5000", {
        reconnection: true,
        origins: "*",
        transports: ["websocket"]
    }
    )},[]);

    useEffect(() => {
        socket.on('chat message', receive);
    },[]);
   
    var send = function() {
        console.log(socket)
        var text = document.getElementById('textAreaChat').value;
        if (text != '') {
            socket.emit('chat message', text);
        } else {
            console.log("message vide");
        }
        document.getElementById('textAreaChat').value = "";
    }
    
    var receive = function(msg) {
        var today = new Date();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var new_div = document.createElement('div')
        new_div.classList.add('new_div');
        new_div.innerHTML = '<a className="ui blue ribbon label">User</a><span><b>'+time+'</b></span><center>'+msg+'</center>'
        if (document.getElementsByClassName("new_div").length>5){
            document.getElementsByClassName("new_div")[0].remove()
        }
        document.getElementsByClassName("ui segment")[2].appendChild(new_div);
    }
    
    return(
        <div className="ui segment">
            <div className="ui five column grid">
                        <div className="ui segment">
                                <div className="ui top attached label">
                                    <div className="ui two column grid">
                                            <div className="column">Chat</div>
                                            <div className="column">
                                                    <div className="ui two column grid">
                                                            <div className="column">{user.login}</div>
                                                            <div className="column"> <i className="user circle icon"></i></div>
                                                            
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                        </div>
                        <div className="ui segment">
                        </div>
                        <div className="ui form">
                            <div className="field">
                                <label>Short Text</label>
                                    <textarea id="textAreaChat" rows="2"></textarea>
                            </div>
                        </div>
                        <button onClick ={send} className="fluid ui right labeled icon button">
                            <i className="right arrow icon"></i>
                            Send
                        </button>
                
            </div>
        </div>
    )
}
export default Chat;
