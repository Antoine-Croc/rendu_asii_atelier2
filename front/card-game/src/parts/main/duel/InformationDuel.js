import axios from 'axios';
function InformationsDuel(props) {
    let idUser = props.idUser;
    let pointAction = props.pointAction;
    
    function endTurn(event) {
        event.preventDefault();
        const order = {idPlayer: idUser, idRoom:props.idRoom };
        console.log(order)
        axios.post('http://127.0.0.1:4000/endTurn', JSON.stringify(order) , { headers: {  "Content-Type": "application/json",'Access-Control-Allow-Origin' :'*'  }})
        
          .then(res => {
            console.log(res.data);
          })
    }
    //TODO : Life ?
    return(
        <div className="row">

            <div className="col-sm-6">
                Vos points actions : {pointAction}
            </div>
            <div className="col-sm-6">
                <form onSubmit={endTurn}><input type="submit" value="endTurn"></input></form>
            </div>
        </div>
    )
}
export default InformationsDuel;