package com.cpe.springboot.chatroom.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.cpe.springboot.chatroom.model.ChatRoomModel;

@Service
public class ChatRoomService {
	
	private final ChatRoomRepository chatroomRepository;
	
	public ChatRoomService(ChatRoomRepository chatroomRepository) {
		this.chatroomRepository = chatroomRepository;		
	}
	
	public ArrayList<ChatRoomModel> getAllRooms(){
		ArrayList<ChatRoomModel> roomList = new ArrayList<>();
		chatroomRepository.findAll().forEach(roomList::add);
		return roomList;
	}

	public ChatRoomModel getRoom(String roomId) {
		Optional<ChatRoomModel> hOpt = chatroomRepository.findById(Integer.valueOf(roomId));
		if (hOpt.isPresent()) {
			return hOpt.get();
		}else {
			return null;
		}
	}
	
	public void addRoom(Integer Id1, Integer Id2) {
		ChatRoomModel cr = new ChatRoomModel(Id1, Id2);
		chatroomRepository.save(cr);
		System.out.println("New room id:" + cr.getRoomId());
	}
	
	public void updateRoom(ChatRoomModel room) {
		chatroomRepository.save(room);
	}
	
	public void deleteRoom(Integer roomId) {
		chatroomRepository.deleteById(roomId);
	}
	
}