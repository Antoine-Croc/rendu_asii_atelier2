
const express = require('express');
const cors = require('cors');
var path = require('path')
const corsOptions ={
  origin:'http://localhost:3000', 
  credentials:true,            //access-control-allow-credentials:true
  optionSuccessStatus:200
}
const app = express();
const http = require('http');
const chatserver = http.createServer(app);
const io = require("socket.io");
const ioServer = io(chatserver);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.use(cors(corsOptions));
app.use(express.static(path.join(__dirname, '/public')));
app.use(function(request, response, next) {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

ioServer.on('connection', function (socket) {
    console.log('a user is connected');
    socket.on('disconnect', function () {
        console.log('a user is disconnected');
    })
    socket.on('chat message', function (msg) {
        console.log('message recu : ' + msg);
        ioServer.emit('chat message', msg);
    })

})

chatserver.listen(5000, () => {
    console.log('Serveur chat connecté sur le port 5000');
});

app.listen(8082, () => {
    console.log('Serveur user connecté sur le port 8082');
})