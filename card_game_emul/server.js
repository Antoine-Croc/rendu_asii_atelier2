const app = require('express')();
const bodyParser = require('body-parser');
const server = require('http').createServer(app);
const io = require('socket.io');
const cors = require ('cors');
const ioServer  = new io.Server(server);

const GameManager = require('./libs/GameManager');
let gameManager;
let gameManagerRooms = new Map();
let gameManagerEmpty;
let id=0;

const connectOptions = {
	'host': 'localhost',
	'port': 61613
};
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());
app.get('/', function(req, res){
	res.sendfile('public/index.html')
});

app.post('/beginAMatch', function (req, res) {
	
	if(req.body.idPlayer != undefined && gameManagerEmpty == undefined && req.body.bet != undefined)
	{
		gameManager = new GameManager();
		gameManager.startAMatch(req.body.idPlayer,id,req.body.bet);
		gameManagerEmpty = id;
		gameManagerRooms.set(id,gameManager)
		console.log("passage par ici ")
		res.json({message:'En attente d\'un adversaire',id:id})
		id += 1;
	}
	else if(req.body.idPlayer != undefined && gameManagerEmpty != undefined && req.body.idRoom == undefined)
	{
		gameManager = gameManagerRooms.get(gameManagerEmpty);
		gameManager.beginAMatch(req.body.idPlayer);
		res.json({message:'La partie va commencer',id:gameManagerEmpty});
		gameManagerRooms.set(req.body.idRoom,gameManager)
		gameManagerEmpty = undefined;
	}
	else if(req.body.idPlayer != undefined && req.body.idRoom != undefined)
	{
		gameManager = gameManagerRooms.get(req.body.idRoom);
		if(gameManager != undefined) 
		{
			gameManager.beginAMatch(req.body.idPlayer);
			res.json({message:'La partie va commencer',id:gameManagerEmpty});
			gameManagerRooms.set(req.body.idRoom,gameManager)
			if(req.body.idRoom == gameManagerEmpty)
			{
				gameManagerEmpty = undefined;
			}
		}
	}
  });

  app.post('/stateOfMatch', function(req, res){
	  if(req.body.idRoom != undefined) 
	  {
		gameManager = gameManagerRooms.get(req.body.idRoom);
		gameManager.stateOfMatch();
		res.send('OK')
	  }
  });

  app.post('/chooseADeck', function(req, res){
	  let ret = false;
	  if(req.body.idRoom != undefined && req.body.idPlayer != undefined && req.body.deckPlayer != undefined) 
	  {
		gameManager = gameManagerRooms.get(req.body.idRoom);
		ret = gameManager.finishYourDeck(req.body.deckPlayer,req.body.idPlayer);
		res.send(ret)
	  }
  });


  app.post('/deck', function(req, res){
	let ret;
	if(req.body.idRoom != undefined && req.body.idPlayer != undefined) 
	{
	  gameManager = gameManagerRooms.get(req.body.idRoom);
	  ret = gameManager.sendADeck(req.body.idPlayer);
	}
	res.send(ret)
});

  app.post('/playACard', function (req, res) {
	let ret = false;
	
	if(req.body.idPlayer != undefined && req.body.idCard != undefined && req.body.idRoom != undefined)
	{
		gameManager = gameManagerRooms.get(req.body.idRoom);
		ret = gameManager.aCardIsPlay(req.body.idPlayer,req.body.idCard)
		gameManagerRooms.set(req.body.idRoom,gameManager);
	}
	if(ret)
	{
		res.send("Card Accepted");
	}
	else 
	{
		res.send("Card Not Accepted");
	}

  });

  app.post('/hitaPlayer', function (req, res) {
	let ret = false;
	
	if(req.body.idPlayer != undefined  &&  req.body.idRoom != undefined)
	{
		gameManager = gameManagerRooms.get(req.body.idRoom);
		console.log(gameManager)
		ret = gameManager.hitAPlayer(req.body.idPlayer);
		gameManagerRooms.set(req.body.idRoom,gameManager)
	}
	if(ret)
	{
		res.send("Hit Accepted");
	}
	else 
	{
		res.send("Hit Not Accepted");
	}

  });


  app.post('/anAttackIsPlay', function (req, res) {
	let ret = false;
	if(req.body.idPlayer != undefined && req.body.idcardStricker != undefined &&req.body.idcardDefenser != undefined &&  req.body.idRoom != undefined)
	{
		
		gameManager = gameManagerRooms.get(req.body.idRoom);
		ret = gameManager.anAttackIsPlay(req.body.idPlayer,req.body.idcardStricker,req.body.idcardDefenser)
		gameManagerRooms.set(req.body.idRoom,gameManager);
	}
	if(ret)
	{
		res.send("Attack Accepted");
	}
	else 
	{
		res.send("Attack Not Accepted");
	}
  });
  
  app.post('/endTurn', function (req, res) {
	if(req.body.idPlayer != undefined && req.body.idRoom != undefined)
	{
		gameManager = gameManagerRooms.get(req.body.idRoom);
		gameManager.endTurn(req.body.idPlayer)
		gameManagerRooms.set(req.body.idRoom,gameManager);
	}
	res.send("Fin du tour.");

  });


ioServer.on('connection', function(socket){
	console.log("A Client is connected")
	if(gameManager != undefined) 
	  {
		  console.log("Send State of Match");
		  gameManager.stateOfMatch();
	  }
	setInterval(() => {
		if(gameManager != undefined)
		{
			if(gameManager.howMuchSockets() != 0)
			{
				let newSocket = gameManager.firstSocketToSend();
				ioServer.emit(newSocket.header, newSocket.msg)
			}
		}
        
	}, 1000);
});

server.listen(4000);
