$(document).ready(function() {
    $('#m').keypress(function(e) {
        if (e.keyCode == 13)
            $('#field').click();
    });
    $.ajax({
        type: "GET",
        url: "http://127.0.0.1:8082/users",
        crossDomain : true
    }).done(function(data) {
        console.log(data)
        try {
            if (data =! "") {
                users = JSON.parse(data)
                addUsersToSelection(users);
            }
        } catch (err) {
            console.log("FAIL")
            console.log(err)
        }
    }).fail(function(jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
    });
});

var users;
var socket = io();

var addUsersToSelection = function(users){
    for (const user in users){
        var new_user = document.createElement('div')
        new_div.classList.add('item');
        new_div.innerHTML = '<div class="item" data-value="'+user+'"><i class="'+user+' user circle icon"></i>'+user+'</div>'
        document.getElementsByClassName("menu")[0].appendChild(new_user);
    }
}
var send = function() {
    var text = $('textarea').val();
    if (text != '') {
        socket.emit('chat message', text);
    } else {
        console.log("message vide");
    }
    $('textarea').val("");
}

var receive = function(msg) {
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var new_div = document.createElement('div')
    new_div.classList.add('new_div');
    new_div.innerHTML = '<a class="ui blue ribbon label">User</a><span><b>'+time+'</b></span><center>'+msg+'</center>'
    if (document.getElementsByClassName("new_div").length>5){
        document.getElementsByClassName("new_div")[0].remove()
    }
    document.getElementsByClassName("ui segment")[2].appendChild(new_div);
}

socket.on('chat message', receive);