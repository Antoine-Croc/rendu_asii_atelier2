package com.cpe.springboot.chatmessage.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class ChatMessageModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer roomId;
	private String timeStamp;
	private String chatmessage;
	private Integer userId;
	
	public ChatMessageModel(String chatmessage, String timeStamp, Integer roomId, Integer userId) {
		this.timeStamp = timeStamp;
		this.chatmessage = chatmessage;		
		this.roomId = roomId;
		this.userId = userId;	
	}
	
	public String getTimeStamp() {
		return timeStamp;
	}
	
	public void setTimeStamp(String ntimeStamp) {
		this.timeStamp = ntimeStamp;
	}
	
	public String getChatMessage() {
		return chatmessage;
	}
	
	public void setChatMessage(String nchatmessage) {
		this.chatmessage = nchatmessage;
	}
	
	public Integer getRoomId() {
		return roomId;
	}
	
	public void setRoomId(Integer nroomId) {
		this.roomId = nroomId;
	}	
	
	public Integer getUserId() {
		return this.userId;
	}
	public void setUserId(Integer nuserId) {
		this.userId = nuserId;
	}
}
