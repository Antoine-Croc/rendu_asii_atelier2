export const updateUser =
    (user) => {
                return { 
                        type: 'UPDATE_USER_ACTION', 
                        user: user 
                       };
              }

export const selectedCard =
    (card) => {
                return { 
                        type: 'SELECTED_CARD_ACTION', 
                        card: card 
                       };
              }

export const updateCards =
(cardlist) => {
                return {
                        type: 'UPDATE_CARDS_ACTION',
                        cardlist:cardlist
                };
}

export const updateIDRoom =
    (idRoom) => {
                return { 
                        type: 'UPDATE_DUEL_ACTION', 
                        idRoom: idRoom 
                       };
              }

export const updateDeck =
    (deck) => {
                return { 
                        type: 'UPDATE_DECK_ACTION', 
                        deck: deck 
                       };
              }